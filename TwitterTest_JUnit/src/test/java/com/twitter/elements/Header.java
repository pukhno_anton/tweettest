package com.twitter.elements;

import com.qatestlab.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by apuhno on 29.06.2016.
 */
@Name("Header")
@FindBy(xpath = "//header | //div[@class='topbar js-topbar']")
public class Header extends BasePage {

    @FindBy(xpath = "//a[@id='user-dropdown-toggle'] | //a[@data-testid='loggedInUserLink']")
    private WebElement profileButton;

    @FindBy(xpath = "//li[@id='signout-button']/button | //a[@href='/logout']")
    private WebElement logoutButton;

    public void clickProfileButton(){
        waitForElementVisibility(profileButton, 10);
        click(profileButton, "Click Profile Button");
    }

    public void clickLogOutButton(){
        click(logoutButton, "Click Logout Button");
    }
}
