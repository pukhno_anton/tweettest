package com.twitter.elements;

import com.qatestlab.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by apuhno on 24.06.2016.
 */
@Name("Login Form")
@FindBy(xpath = "//form[@class='t1-form signin' or @class='signin-form']")
public class LoginForm extends BasePage {

    @Name("Email Input")
    @FindBy(xpath = "//input[@id='signin-email' or @id='session[username_or_email]']")
    private WebElement emailInput;

    @Name("Password Input")
    @FindBy(xpath = "//input[@id='signin-password' or @id='session[password]']")
    private WebElement passwordInput;

    @Name("Log In Button")
    @FindBy(xpath = "//button[@id='signupbutton' or @class='submit btn primary-btn flex-table-btn js-submit']")
    private WebElement loginButton;

    public void typeEmail(String email) {
        clearAndSendKeys(emailInput, email, "Type email: " + email);
    }

    public void typePassword(String password) {
        clearAndSendKeys(passwordInput, password, "Type password: " + password);
    }

    public void clickLogInTweet() {
        click(loginButton, "Click Log In Button");
    }
}
