package com.twitter.elements;

import com.qatestlab.base.BasePage;
import com.qatestlab.base.BaseTest;
import com.qatestlab.reporter.Reporter;
import com.qatestlab.staticProvider.Tweet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

/**
 * Created by apuhno on 27.06.2016.
 */
@Name("Timeline Container")
@FindBy(id = "timeline")
public class TimelineDiv extends BasePage {

    @FindBy(css = ".tweet .js-tweet-text")
    private WebElement lastTweet;

    @FindBy(xpath = "//button[@class='ProfileTweet-actionButton  js-actionButton js-actionRetweet']")
    private WebElement lastTweetRetweetButton;

    @FindAll({
            @FindBy(xpath = "//li[@data-item-type='tweet']")
    })
    private List<WebElement> tweetsList;

    @FindBy(xpath = "//button[@class='btn-link back-to-top hidden']")
    private WebElement buttonGoToTop;

    private final String xpathTweetParagraph = "//p[text()='%s']";
    private final String xpathRetweetButton = "//div[contains(p, '%s')]/following-sibling::div[3]/descendant::button[@class='ProfileTweet-actionButton  js-actionButton js-actionRetweet']";

    private RetweetModalDialog retweetModalDialog;

    public String getLastTweetText() {
        return lastTweet.getText();
    }

    public void clickLastRetweetButton() {
        click(lastTweetRetweetButton, "Click Last Retweet Button");
    }

    public void scrollToTweet(Tweet tweet) {
        WebElement element = BaseTest.driver().findElement(By.xpath(String.format(xpathTweetParagraph, tweet.getTweet())));
        scrollToElement(element, "Scroll to tweet " + tweet.getTweet());
    }

    public void retweet(Tweet tweet) throws Exception {
        WebElement element = BaseTest.driver().findElement(By.xpath(String.format(xpathRetweetButton, tweet.getTweet())));
        javascriptClick(element, "Click retweet: " + tweet.getTweet());
    }

    public String getTweetsCount() {
        int tweetsCount = tweetsList.size();
        Reporter.Log("Count tweets = " + tweetsCount);
        return String.valueOf(tweetsCount);
    }

    public void tweetScroll() {
        scrollToElement(buttonGoToTop, "Scrolling To Bottom");
    }
}
