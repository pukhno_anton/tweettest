package com.twitter.elements;

import com.qatestlab.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by apuhno on 27.06.2016.
 */
@Name("Profile Card")
@FindBy(xpath = "//div[@class='DashboardProfileCard  module']")
public class ProfileCard extends BasePage {

    @FindBy(xpath = "//span[@class='ProfileCardStats-statValue']")
    private WebElement tweetNumSpan;

    public String getTweetNumFromSpan() {
        return tweetNumSpan.getText();
    }
}
