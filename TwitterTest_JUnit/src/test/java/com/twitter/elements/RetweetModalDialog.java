package com.twitter.elements;

import com.qatestlab.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by apuhno on 27.06.2016.
 */
@Name("Retweet Modal Dialog")
@FindBy(id = "retweet-tweet-dialog-dialog")
public class RetweetModalDialog extends BasePage {

    @FindBy(xpath = "//button[@class='btn primary-btn retweet-action']")
    private WebElement retweetConfirmationButton;

    public void clickRetweetButtonInConfirmationWindow(){
        click(retweetConfirmationButton, "Click Retweet Confirmation Button");
    }
}
