package com.twitter.elements;

import com.qatestlab.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by apuhno on 29.06.2016.
 */
@Name("Mobile Front Button Container")
@FindBy(xpath = "//div[@class='Front-buttonContainer']")
public class MobileFrontButtonDiv extends BasePage {

    @FindBy(xpath = "//a[@class=' Button Button--default  Button--invert ']")
    private WebElement loginLink;

    public void clickLoginLink(){
        click(loginLink, "Click Login Link Mobile");
    }
}
