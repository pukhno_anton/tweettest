package com.twitter.elements;

import com.qatestlab.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;

/**
 * Created by apuhno on 27.06.2016.
 */
@Name("New Tweet Modal Dialog")
@FindBy(id = "global-tweet-dialog-dialog")
public class NewTweetModalDialog extends BasePage {

    @FindBy(id = "tweet-box-global")
    private WebElement tweetBox;

    @FindBy(xpath = "//button[@class='btn primary-btn tweet-action tweet-btn js-tweet-btn']")
    private WebElement tweetButton;

    public void writeTweet(String tweet) {
        clearAndSendKeys(tweetBox, tweet, "Write Tweet: " + tweet);
    }

    public void sendTweet() {
        click(tweetButton, "Send Tweet");
    }
}
