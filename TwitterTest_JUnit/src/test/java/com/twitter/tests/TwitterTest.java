package com.twitter.tests;

import com.qatestlab.base.BaseTest;
import com.qatestlab.reporter.Reporter;
import com.qatestlab.staticProvider.Serialization;
import com.qatestlab.staticProvider.StaticProvider;
import com.qatestlab.staticProvider.Tweet;
import com.twitter.pages.HelloPage;
import com.twitter.pages.LoggedInPage;
import com.twitter.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.remote.BrowserType;
import ru.yandex.qatools.allure.annotations.Title;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

/**
 * Created by apuhno on 13.06.2016.
 */
@RunWith(Parameterized.class)
public class TwitterTest extends BaseTest {

    private String firstemail;
    private String firstpassword;
    private String secondemail;
    private String secondpassword;

    public TwitterTest(String firstemail, String firstpassword, String secondemail, String secondpassword) {
        this.firstemail = firstemail;
        this.firstpassword = firstpassword;
        this.secondemail = secondemail;
        this.secondpassword = secondpassword;
    }

    @Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(StaticProvider.data2());
    }

    String randomStr;

    @Title("Tweet Test")
    @Test
    public void TwitterTest() throws Exception {
        LoginPage loginPage;
        if(System.getProperty("browser").equals(BrowserType.ANDROID) || System.getProperty("browser").equals(BrowserType.IPHONE) ){
            HelloPage helloPage = new HelloPage();
            loginPage = helloPage.clickLoginLink();
        }
        else
            loginPage = new LoginPage();
        LoggedInPage loggedInPage = loginPage.login(firstemail, firstpassword);

        loggedInPage.clickTweetButton();
        randomStr = UUID.randomUUID().toString();
        loggedInPage.writeTweet(randomStr);
        loggedInPage.sendTweet();

        Tweet tweet = new Tweet();
        tweet.setTweet(randomStr);
        Serialization.write(tweet, "tweet.xml");

        loggedInPage.clickProfileButton();
        loggedInPage.clickLogoutButton();
    }
}
