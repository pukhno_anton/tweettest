package com.twitter.pages;

import com.qatestlab.base.BasePage;
import com.qatestlab.base.BaseTest;
import com.qatestlab.staticProvider.Tweet;
import com.twitter.elements.RetweetModalDialog;
import com.twitter.elements.TimelineDiv;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

/**
 * Created by apuhno on 13.06.2016.
 */
public class OtherUsersPage extends BasePage {

    private TimelineDiv timelineDiv;
    private RetweetModalDialog retweetModalDialog;

    public OtherUsersPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(BaseTest.driver())), this);
    }

    public String getLastTweetText() {
        return timelineDiv.getLastTweetText();
    }

    public OtherUsersPage clickLastRetweetButton() {
        timelineDiv.clickLastRetweetButton();
        return this;
    }

    public OtherUsersPage clickRetweetButtonInConfirmationWindow() {
        retweetModalDialog.clickRetweetButtonInConfirmationWindow();
        return this;
    }

    public OtherUsersPage scrollToTweet(Tweet tweet) {
        timelineDiv.scrollToTweet(tweet);
        return this;
    }

    public OtherUsersPage retweet(Tweet tweet) throws Exception {
        timelineDiv.retweet(tweet);
        return this;
    }
}
