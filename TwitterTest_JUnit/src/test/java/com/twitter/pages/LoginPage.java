package com.twitter.pages;

import com.qatestlab.base.BasePage;
import com.qatestlab.base.BaseTest;
import com.twitter.elements.LoginForm;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

/**
 * Created by apuhno on 13.06.2016.
 */
public class LoginPage extends BasePage {

    //@FindBy(xpath = "//form[@class='t1-form signin']")
    private LoginForm loginForm;

    public LoginPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(BaseTest.driver())), this);
    }

    public LoginPage typeEmail(String email) {
        loginForm.typeEmail(email);
        return this;
    }

    public LoginPage typePass(String pass) {
        loginForm.typePassword(pass);
        return this;
    }

    public LoggedInPage pressLoginButton() {
        loginForm.clickLogInTweet();
        return new LoggedInPage();
    }

    public LoggedInPage login(String email, String pass) {
        this.typeEmail(email);
        this.typePass(pass);
        this.pressLoginButton();
        return new LoggedInPage();
    }
}
