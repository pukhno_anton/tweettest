package com.twitter.pages;

import com.qatestlab.base.BasePage;
import com.qatestlab.base.BaseTest;
import com.twitter.elements.Header;
import com.twitter.elements.NewTweetModalDialog;
import com.twitter.elements.ProfileCard;
import com.twitter.elements.TimelineDiv;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

import java.util.UUID;

/**
 * Created by apuhno on 13.06.2016.
 */
public class LoggedInPage extends BasePage {

    private TimelineDiv timelineDiv;
    private NewTweetModalDialog newTweetModalDialog;
    private ProfileCard profileCard;
    private Header header;

    @FindBy(xpath = "//a[@href='/compose/tweet'] | //button[@id='global-new-tweet-button']")
    private WebElement tweetButton;

    public LoggedInPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(BaseTest.driver())), this);
    }

    public LoggedInPage clickTweetButton() {
        click(tweetButton, "Click Tweet Button");
        return this;
    }

    public LoggedInPage writeTweet(String tweet) {
        newTweetModalDialog.writeTweet(tweet);
        return this;
    }

    public LoggedInPage sendTweet() {
        newTweetModalDialog.sendTweet();
        invisibilityOfElementWait(newTweetModalDialog, 10);
        return this;
    }

    public LoggedInPage sendRandomTweets(int count){
        for(int i=0; i<count; i++){
            clickTweetButton();
            writeTweet(UUID.randomUUID().toString());
            sendTweet();
        }
        return this;
    }

    public LoginPage firstLeaveSecondIn() {
        deleteCookies();
        invisibilityOfElementWait(newTweetModalDialog, 10);
        navigateToUrl("https://twitter.com/");
        return new LoginPage();
    }

    public OtherUsersPage goTo(String url) {
        navigateToUrl(url);
        return new OtherUsersPage();
    }

    public String getTweetNumFromSpan() {
        return profileCard.getTweetNumFromSpan();
    }

    public void tweetScroll() {
        timelineDiv.tweetScroll();
    }

    public String getTweetsCount() {
        return timelineDiv.getTweetsCount();
    }

    public LoggedInPage clickProfileButton(){
        header.clickProfileButton();
        return this;
    }

    public LoginPage clickLogoutButton(){
        header.clickLogOutButton();
        return new LoginPage();
    }
}
