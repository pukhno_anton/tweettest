package com.twitter.pages;

import com.qatestlab.base.BasePage;
import com.qatestlab.base.BaseTest;
import com.twitter.elements.MobileFrontButtonDiv;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

/**
 * Created by apuhno on 29.06.2016.
 */
public class HelloPage extends BasePage {

    private MobileFrontButtonDiv mobileFrontButtonDiv;

    public HelloPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(BaseTest.driver())), this);
    }

    public LoginPage clickLoginLink(){
        mobileFrontButtonDiv.clickLoginLink();
        return new LoginPage();
    }
}
