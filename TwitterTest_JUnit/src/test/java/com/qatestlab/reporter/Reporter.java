package com.qatestlab.reporter;

import com.qatestlab.base.BaseTest;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by apuhno on 14.06.2016.
 */
public class Reporter {
    @Step("{0}")
    public static void Log(String value) {
        //empty method
    }

    @Attachment(value = "Screenshot", type = "image/png")
    public static byte[] saveScreenShot() {
        return ((TakesScreenshot) BaseTest.driver()).getScreenshotAs(OutputType.BYTES);
    }
}
