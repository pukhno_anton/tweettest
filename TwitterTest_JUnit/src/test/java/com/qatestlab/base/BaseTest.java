package com.qatestlab.base;

import com.qatestlab.reporter.Reporter;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by apuhno on 13.06.2016.
 */
public class BaseTest {

    private static ThreadLocal<WebDriver> driverThread = new ThreadLocal<>();

    public static WebDriver driver() {
        return driverThread.get();
    }

    @Rule
    public TestWatcher screenshotOnFailure = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            Reporter.saveScreenShot();
        }
    };

    @BeforeClass
    public static void setUp() throws Exception {
        final String USERNAME = "vinni482";
        final String ACCESS_KEY = "c1d83fd5-7502-451d-ae2d-97fc7684a53b";
        final String address = "http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";

//        if (capability(System.getProperty("browser")).equals(DesiredCapabilities.android()))
//            address = "http://localhost:4444/wd/hub";
//        else if (capability(System.getProperty("browser")).equals(DesiredCapabilities.iphone()))
//            address = "http://192.168.0.106:4444/wd/hub";
//        else
//            address = "http://localhost:4445/wd/hub";

        driverThread.set(new RemoteWebDriver(new URL(address), capability(System.getProperty("browser"))));
        driver().get("https://twitter.com/");
        if (!capability(System.getProperty("browser")).equals(DesiredCapabilities.android()) && !capability(System.getProperty("browser")).equals(DesiredCapabilities.iphone())) {
            driver().manage().window().maximize();
        }
        driver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        System.setProperty("org.uncommons.reportng.escape-output", "false");
    }

    static DesiredCapabilities capability(String browser) {
        switch (browser) {
            case BrowserType.FIREFOX: //FireFox
            {
                DesiredCapabilities caps = DesiredCapabilities.firefox();
                caps.setCapability("platform", "Windows XP");
                caps.setCapability("version", "43.0");
                return caps;
            }
            case BrowserType.CHROME: //Chrome
            {
                DesiredCapabilities caps = DesiredCapabilities.chrome();
                caps.setCapability("platform", "Windows 7");
                caps.setCapability("version", "43.0");
                return caps;
            }
            case BrowserType.ANDROID: {
                DesiredCapabilities caps = DesiredCapabilities.android();
                /*caps.setCapability("platform", "Windows 7");
                caps.setCapability("version", "43.0");*/
                return caps;
            }
            case BrowserType.IPHONE: {
                DesiredCapabilities caps = DesiredCapabilities.iphone();
                /*caps.setCapability("platform", "Windows XP");
                caps.setCapability("version", "43.0");*/
                return caps;
            }
            default:
            {
                DesiredCapabilities caps = DesiredCapabilities.firefox();
                caps.setCapability("platform", "Windows XP");
                caps.setCapability("version", "43.0");
                return caps;
            }
        }
    }

    @AfterClass
    public static void tearDown() {
        driver().quit();
    }
}
