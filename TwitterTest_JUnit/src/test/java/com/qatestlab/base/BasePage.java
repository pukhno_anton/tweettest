package com.qatestlab.base;

import com.qatestlab.reporter.Reporter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.Arrays;

/**
 * Created by apuhno on 14.06.2016.
 */
public class BasePage extends HtmlElement {
    public void invisibilityOfElementWait(WebElement element, long timeOut) {
        (new WebDriverWait(BaseTest.driver(), timeOut)).until(ExpectedConditions.invisibilityOfAllElements(Arrays.asList(element)));
    }

    public void waitForElementVisibility(WebElement element, long timeOut){
        (new WebDriverWait(BaseTest.driver(), timeOut)).until(ExpectedConditions.visibilityOf(element));
    }

    public void clearAndSendKeys(WebElement element, String text, String message) {
        Reporter.Log(message);
        element.clear();
        element.sendKeys(text);
    }

    public void click(WebElement element, String message) {
        Reporter.Log(message);
        element.click();
    }

    public void javascriptClick(WebElement element, String message) {
        ((JavascriptExecutor) BaseTest.driver()).executeScript("arguments[0].click();", element);
        Reporter.Log(message);
    }

    public void deleteCookies() {
        BaseTest.driver().manage().deleteAllCookies();
    }

    public void navigateToUrl(String url) {
        BaseTest.driver().get(url);
    }

    public void scrollDown() {
        ((JavascriptExecutor) BaseTest.driver()).executeScript("window.scrollTo(0,document.body.scrollHeight);");
    }

    public void scrollToElement(WebElement element, String message) {
        Reporter.Log(message);
        do {
            scrollDown();
            if (element.isDisplayed())
                break;
        } while (true);
    }
}
