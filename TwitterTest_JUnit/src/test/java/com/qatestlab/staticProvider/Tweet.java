package com.qatestlab.staticProvider;

/**
 * Created by apuhno on 23.06.2016.
 */
public class Tweet {
    private String tweet;

    public void setTweet(String s) {
        tweet = s;
    }

    public String getTweet() {
        return tweet;
    }
}
