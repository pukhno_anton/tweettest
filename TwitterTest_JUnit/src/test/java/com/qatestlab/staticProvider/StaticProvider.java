package com.qatestlab.staticProvider;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by apuhno on 13.06.2016.
 */
public class StaticProvider {
    public static Object[][] data1() {
        return getExcelData(System.getProperty("exceldata.dir") + "data.xls", "FirstSheet");
    }

    public static Object[][] data2() {
        return getXmlData(System.getProperty("xmldata.dir") + "data.xml");
    }

    public static String[][] getXmlData(String fileName) {
        String[][] arrayXmlData = null;
        try {
            File xmlFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);

            doc.getDocumentElement().normalize();

            Node accounts = doc.getElementsByTagName("accounts").item(0);

            arrayXmlData = new String[1][accounts.getChildNodes().getLength() / 2];

            int count = 0;
            for (int i = 1; i < accounts.getChildNodes().getLength(); i += 2) {
                arrayXmlData[0][count] = accounts.getChildNodes().item(i).getTextContent();
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayXmlData;
    }

    public static String[][] getExcelData(String fileName, String sheetName) {
        String[][] arrayExcelData = null;
        try {
            FileInputStream fs = new FileInputStream(fileName);
            Workbook wb = Workbook.getWorkbook(fs);
            Sheet sh = wb.getSheet(sheetName);

            int totalNoOfCols = sh.getColumns();
            int totalNoOfRows = sh.getRows();

            arrayExcelData = new String[totalNoOfRows - 1][totalNoOfCols];

            for (int i = 1; i < totalNoOfRows; i++) {

                for (int j = 0; j < totalNoOfCols; j++) {
                    arrayExcelData[i - 1][j] = sh.getCell(j, i).getContents();
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            e.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
        return arrayExcelData;
    }
}
