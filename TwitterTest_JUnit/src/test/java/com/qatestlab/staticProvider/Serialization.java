package com.qatestlab.staticProvider;

import com.qatestlab.reporter.Reporter;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by apuhno on 23.06.2016.
 */
public class Serialization {
    public static void write(Tweet tweet, String filename) throws Exception{
        try(XMLEncoder encoder =
                    new XMLEncoder(
                            new BufferedOutputStream(
                                    new FileOutputStream(filename))))
        {
            encoder.writeObject(tweet);
            encoder.close();
            Reporter.Log("Tweet: " + tweet.getTweet() + " serialized in file " + filename);
        }
    }

    public static Tweet read(String filename) throws Exception {
        try(XMLDecoder decoder =
                new XMLDecoder(new BufferedInputStream(
                        new FileInputStream(filename))))
        {
            Tweet o = (Tweet) decoder.readObject();
            decoder.close();
            Reporter.Log("Tweet: " + o.getTweet() + " deserialized from file " + filename);
            return o;
        }
    }
}
